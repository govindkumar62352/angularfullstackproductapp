export class Booking {
    // Id?:number
    // ProductId?:number
    // Name?:string 
    // Description ?:string
    // Price ?:number
    // ImagePath?:string
    // Username?:string
    id?: number
    productId?: number
    userName?: string
    name?: string
    description?: string
    price?: number
    imagePath?: string
}
