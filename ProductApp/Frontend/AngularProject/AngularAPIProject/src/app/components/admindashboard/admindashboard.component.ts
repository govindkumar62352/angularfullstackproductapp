import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/service/product.service';

@Component({
  selector: 'app-admindashboard',
  templateUrl: './admindashboard.component.html',
  styleUrls: ['./admindashboard.component.css']
})
export class AdmindashboardComponent implements OnInit {
  products?:Product[]

  constructor(private productService:ProductService) { 
    console.log("This is dashboard Constructor");
  }

  ngOnInit(): void {
    console.log("This is OnItMethod");
    this.productService.getAllProducts().subscribe(res=>{
      console.log(res);
      this.products=res;
  });
}
}
