import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Booking } from 'src/app/models/booking';
import { BookingService } from 'src/app/service/booking.service';
import { ProductService } from 'src/app/service/product.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-addtocart',
  templateUrl: './addtocart.component.html',
  styleUrls: ['./addtocart.component.css']
})
export class AddtocartComponent implements OnInit {

  UserName?: string
  productId = 0
  booking: Booking
  constructor(private route: ActivatedRoute, private bookingService: BookingService, private productService: ProductService,private router:Router) {
    this.booking = new Booking();

  }

  ngOnInit(): void {
    this.route.params.forEach(data => {
      this.UserName = data['Username'];
      //console.log(typeof(data['Id']));
     // console.log(typeof(this.productId));
      this.productId= Number( data['Id'])
     // console.log(typeof(this.productId));
      this.AddToCart()
    })

  }

  AddToCart() {
    this.productService.getProductById(this.productId).subscribe(res => {
      this.booking.userName = this.UserName
      this.booking.productId = this.productId
      this.booking.name = res['name']
      this.booking.description = res['description']
      this.booking.price = res['price']
      this.booking.imagePath = res['imagePath']
      console.log(this.booking)
      console.log("Before Cart")

      this.bookingService.AddToCart(this.booking).subscribe(res1 => {
        if (res1) {
          console.log("Added To Cart");
          Swal.fire(
            'Card Added ',
            'Card Added Success',
            'success'
          )
          this.router.navigateByUrl(`/dashboard/${this.UserName}`)

        }
      })

    })

  }



}
