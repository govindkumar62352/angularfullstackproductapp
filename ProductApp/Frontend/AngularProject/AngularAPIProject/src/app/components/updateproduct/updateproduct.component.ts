import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/service/product.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-updateproduct',
  templateUrl: './updateproduct.component.html',
  styleUrls: ['./updateproduct.component.css']
})
export class UpdateproductComponent implements OnInit {
  productId = 0

  edit = new FormGroup(
    {
      name: new FormControl(),
      description: new FormControl(),
      price: new FormControl(),
      imagePath: new FormControl()
    }
  )
  product: Product
  constructor(private productService: ProductService, private _Activatedroute: ActivatedRoute, private router: Router) {
    this.product = new Product()

  }

  ngOnInit(): void {
    this.GetProductById();
  }
  GetProductById() {
    this._Activatedroute.params.subscribe(result => {
      this.productId = result['id']
      console.log(this.productId);
      
      this.productService.getProductById(this.productId).subscribe(res => {

        this.edit = new FormGroup(
          {
            name: new FormControl(res['name']),
            description: new FormControl(res['description']),
            price: new FormControl(res['price']),
            imagePath: new FormControl(res['imagePath'])
          }
        )



      });
    })

  }
  updateProduct(product: Product) {
    this.productService.updateProduct(this.productId, product).subscribe(res => {
      if (res) {
        Swal.fire(
          'Update Product',
          'Product Update Successfully',
          'success'
        )
        this.router.navigateByUrl("/admindashboard")

      } else {
        Swal.fire(
          'Update Product',
          'Product Fail',
          'error'
        )


      }

    })

  }

}
