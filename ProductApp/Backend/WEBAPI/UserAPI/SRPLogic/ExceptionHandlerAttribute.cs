﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using ProductWebApplication.MVC.Exceptions;
using UserAPI.Exception;

namespace ProductWebApplication.MVC.ExceptionAttribute
{
    public class ExceptionHandlerAttribute:ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception.GetType() == typeof(UserAlreadyExistException))
            {
                context.Result = new OkObjectResult(context.Exception.Message);
            }
            
            else if(context.Exception.GetType()==typeof(UserCredentialInvalidException))
            {
                context.Result = new OkObjectResult(context.Exception.Message);
            }
            else
            {
                context.Result = new StatusCodeResult(500);
            }
        }
    }
}
