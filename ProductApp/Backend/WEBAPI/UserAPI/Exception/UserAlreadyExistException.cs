﻿namespace UserAPI.Exception
{
    public class UserAlreadyExistException:ApplicationException
    {
        public UserAlreadyExistException()
        {

        }
        public UserAlreadyExistException(string msg) : base(msg)
        {

        }
    }
}
